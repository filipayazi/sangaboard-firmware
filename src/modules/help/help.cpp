#include "config.h"
#ifdef HELP
#include "help.h"
#include <ComPort.h>

void help_setup()
{
    register_module(help_commands, NULL);
}

void help(String command)
{
    comPort->println("");
    comPort->print("Board: ");
    comPort->println(F(BOARD_STRING));

#ifdef LIGHT_SENSOR
#if defined ADAFRUIT_TSL2591
    comPort->println(F("Compiled with Adafruit TSL2591 support"));
#elif defined ADAFRUIT_ADS1115
    comPort->println(F("Compiled with Adafruit ADS1115 support"));
#endif
#endif //LIGHT_SENSOR


#ifdef ENDSTOPS
#ifdef ENDSTOPS_MIN
    comPort->println(F("Compiled with min endstops support"));
#endif
#ifdef ENDSTOPS_MAX
    comPort->println(F("Compiled with max endstops support"));
#endif
#endif //ENDSTOPS


    comPort->println("");
    comPort->println(F("Commands (terminated by a newline character):"));
#ifdef STAGE
    comPort->println(F("mrx <d>                        - relative move in x"));
    comPort->println(F("mry <d>                        - relative move in y"));
    comPort->println(F("mrz <d>                        - relative move in z"));
    comPort->println(F("mr <d> <d> <d>                 - relative move in all 3 axes"));
    comPort->println(F("release                        - de-energise all motors"));
    comPort->println(F("p?                             - print position (3 space-separated integers"));
    comPort->println(F("ramp_time <d>                  - set the time taken to accelerate/decelerate in us"));
    comPort->println(F("min_step_delay <d>             - set the minimum time between steps in us."));
    comPort->println(F("dt <d>                         - set the minimum time between steps in us."));
    comPort->println(F("ramp_time?                     - get the time taken to accelerate/decelerate in us"));
    comPort->println(F("min_step_delay?                - get the minimum time between steps in us."));
    comPort->println(F("zero                           - set the current position to zero."));
    comPort->println(F("stop                           - stop a move in progress."));
    comPort->println(F("blocking_moves?                - get blocking moves enabled"));
    comPort->println(F("blocking_moves <bool>          - enable/disable blocking moves"));
    comPort->println(F("moving?                        - check if a move is in progress"));
    comPort->println(F("notify_on_stop                 - respond with stopped when current move finishes"));
#endif //STAGE

#ifdef LIGHT_SENSOR
    comPort->println(F("light_sensor_gain <d>          - set the gain of the light sensor"));
    comPort->println(F("light_sensor_gain?             - get the gain of the light sensor"));
    comPort->println(F("light_sensor_gain_values?      - get the allowable gain values of the light sensor"));
    comPort->println(F("light_sensor_integration_time? - get the integration time in milliseconds"));
    comPort->println(F("light_sensor_intensity?        - read the current value from the full spectrum diode"));
#endif //LIGHT_SENSOR

#if defined(ENDSTOPS_MIN) || defined(ENDSTOPS_MAX)
    comPort->println(F("endstops?                      - get triggered endstops in (1,0,-1) format for max, none, min"));
    comPort->println(F("home_min <axes?>               - home given (00000zyx byte, e.g. 1 for x) or all axes to their min position"));
    comPort->println(F("home_max <axes?>               - home given (00000zyx byte, e.g. 3 for x and y) or all axes to their max position"));
    comPort->println(F("max_p?                         - return positions of max endstops"));
    comPort->println(F("max <d> <d> <d>                - set maximum positions"));
#endif

#ifdef STEPSTICK
    comPort->println(F("stepstick_move <f> <d>         - move stepper by <d> with max speed <f>"));
    comPort->println(F("stepstick_release              - release stepstick stepper"));
    comPort->println(F("stepstick_stop                 - abort stepstick move"));
#endif
    //comPort->println(F("test_mode <s>                  - set test_mode <on> <off>"));
    comPort->println(F("version                        - get firmware version string"));
    comPort->println("");
    comPort->println("Input Key:");
    comPort->println(F("<d>                            - a decimal integer."));
    comPort->println("");
    comPort->println("--END--");
}
#endif //HELP