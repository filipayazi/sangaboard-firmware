/*
 * Sangaboard firmware
 *
 * Refactored from bath_open_instrumentation_group/sangaboard/arduino_code
 *
 * This firmware was written by
 * Richard Bowman
 * Julian Stirling
 * Boyko Vodenicharski
 * Filip Ayazi
 *
 * Much of the code is based on older code written by
 * James Sharkey and Fergus Riche
 *
 * Released under GPL v3, 2021
 */

#include "main.h"
#include "config.h"
#include <Arduino.h>
#include <stdint.h>
#include <ComPort.h>
#ifdef SUPPORT_EEPROM
#include <EEPROM.h>
#else
#include "dummyEEPROM.h"
#endif

#ifdef HELP
    #include "modules/help/help.h"
#endif

#ifdef STAGE
    #include "modules/stage/stage.h"
#endif

#ifdef ENDSTOPS
    #include "modules/endstops/endstops.h"
#endif

#ifdef STEPSTICK
    #include "modules/stepstick/stepstick.h"
#endif

#ifdef ILLUMINATION
    #include "modules/illumination/illumination.h"
#endif

#ifdef LIGHT_SENSOR
    #include "modules/light_sensor/light_sensor.h"
#endif

const Command *registered_commands[MAX_COMMANDS];
void (*registered_loop_functions[MAX_MODULES])(void);

uint8_t registered_commands_count = 0;
uint8_t registered_loop_fn_count = 0;

const Command core_commands[] = {
    {"version", get_version},
    {"board", get_board},
    {"list_modules", get_modules},
    {"eeprom_last_commit", get_eeprom_last_commit},
    {"system_status", get_system_status},
    {"eeprom_commit", eeprom_commit},
    END_COMMAND};

void register_module(const Command commands[], void (*loop_fn)(void))
{
    for (int i = 0; CHECK_END_COMMAND(commands[i]); i++)
        registered_commands[registered_commands_count++] = &commands[i];

    if (loop_fn)
        registered_loop_functions[registered_loop_fn_count++] = loop_fn;
}

void handle_command(String received_command)
{
    for (int i = 0; CHECK_END_COMMAND(*registered_commands[i]); i++)
    {
        if (received_command.startsWith(registered_commands[i]->command))
            return registered_commands[i]->run(received_command.substring(strlen(registered_commands[i]->command)));
    }

#ifdef HELP
    comPort->print(F("Command not recognised: '"));
    comPort->print(received_command);
    comPort->println(F("', Type 'help' for a list of commands."));
#else
    comPort->println(F("Invalid command"));
#endif
}

uint8_t parse_arguments(char **arguments, String command, uint8_t max_args)
{
    char buffer[MAX_ARGUMENT_LENGTH];

    uint8_t from = 0;
    while (command[from] == ' ' && from < command.length())
        from++;

    uint8_t parsed = 0;
    while (from < command.length() && parsed < max_args)
    {
        int space = command.indexOf(' ', from);
        if (space == -1)
            space = command.length();
        memcpy(buffer, &command[from], space - from);
        buffer[space - from] = '\0';
        arguments[parsed++] = strdup(buffer);
        from = space + 1;
    }

    return parsed;
}

void print_arg_difference(uint8_t parsed, uint8_t expected)
{
    comPort->print(F("Got "));
    comPort->print(parsed);
    comPort->print(F(" but expected "));
    comPort->print(expected);
}

void free_parsed_arguments(char **arguments, uint8_t parsed)
{
    // free parsed arguments
    for (uint8_t i = 0; i < parsed; i++)
    {
        free(arguments[i]);
    }
}

bool assert_exact_args(uint8_t parsed, uint8_t num_args)
{
    return parsed == num_args;
}

void get_version(String)
{
    comPort->println(F(VERSION_STRING));
    return;
}

void get_board(String)
{
    comPort->println(F(BOARD_STRING));
    return;
}

void get_modules(String)
{
#if defined(LIGHTSENSOR) && defined(LIGHT_SENSOR_ADAFRUIT_TSL2591)
    comPort->println(F("Light Sensor: TSL2591"));
#elif defined(LIGHTSENSOR) && defined(LIGHT_SENSOR_ADAFRUIT_ADS1115)
    comPort->println(F("Light Sensor: ADS1115"));
#endif

#if defined(ENDSTOPS)
    comPort->print("Endstops:");
    #ifdef ENDSTOPS_MIN
    comPort->print(" min");
    #endif
    #ifdef ENDSTOPS_MAX
    comPort->print(" max");
    #endif
    #ifdef ENDSTOPS_SOFT
    comPort->print(" soft");
    #endif
    comPort->println();
#endif
#if defined(ILLUMINATION)
    comPort->println("Illumination:default");
#endif
    comPort->println("--END--");
}

#if defined(SUPPORT_EEPROM) && defined(MCU_PICO)
unsigned long eeprom_last_commit;
void eeprom_commit_at_interval(unsigned long interval)
{
    unsigned long next_commit = eeprom_last_commit + interval;
    unsigned long now = millis();
    // millis() can overflow - this if statement will trigger
    // every `interval` accounting for overflows.
    if ( (now > next_commit && next_commit > eeprom_last_commit) // no overflow
         || (now > next_commit && now < eeprom_last_commit)) // overflow
    {
        EEPROM.commit();
        eeprom_last_commit = millis();
    }
}
#endif

void eeprom_commit(String)
{
#if defined(SUPPORT_EEPROM) && defined(MCU_PICO)
    EEPROM.commit();
    eeprom_last_commit = millis();
    comPort->println("EEPROM updated");
#else
    comPort->println("Not using pico");
#endif
}

void get_eeprom_last_commit(String)
{
#if defined(SUPPORT_EEPROM) && defined(MCU_PICO)
    comPort->println(eeprom_last_commit);
#else
    comPort->println("Not using pico");
#endif
    return;
}

void get_system_status(String)
{
    comPort->println(F(BOARD_STRING));
    comPort->println(F(VERSION_STRING));
    #ifdef MCU_PICO
        comPort->print(F("Heap remaining:"));
        comPort->println(rp2040.getFreeHeap());
    #endif
}

#ifndef UNIT_TEST // don't define setup() and loop() for unit testing
void setup()
{
    #if defined(SUPPORT_EEPROM) && defined(MCU_PICO)
        // On the Pico, EEPROM is emulated.
        // This means we need to explicitly enable it, and also
        // call EEPROM.commit() periodically to persist it to flash.
        EEPROM.begin(EMULATED_EEPROM_SIZE);
        eeprom_last_commit = millis();
    #endif
    // initialise serial port
    #ifdef WIRING_SERIAL_TX
        Serial2.setTX(WIRING_SERIAL_TX);
        Serial2.setRX(WIRING_SERIAL_RX);
    #endif

    comPort = (HardwareSerial*) &PRIMARY_SERIAL_PORT;
    PRIMARY_SERIAL_PORT.begin(115200);
    #ifdef SECONDARY_SERIAL_PORT
        SECONDARY_SERIAL_PORT.begin(115200);
        //wait for either port to be ready
        while (!PRIMARY_SERIAL_PORT && !SECONDARY_SERIAL_PORT)
    #else
        while (!PRIMARY_SERIAL_PORT)
    #endif
        delay(1);

    register_module(core_commands, NULL);
    #ifdef HELP
    help_setup();
    #endif

    //register modules
    #ifdef STAGE
    stage_setup();
    #endif

    #ifdef LIGHT_SENSOR
    light_sensor_setup();
    #endif

    #ifdef ENDSTOPS
    endstops_setup();
    #endif

    #ifdef STEPSTICK
    stepstick_setup();
    #endif

    #ifdef ILLUMINATION
    illumination_setup();
    #endif

    registered_commands[registered_commands_count] = &end_command;
    comPort->println(F(VERSION_STRING));
}

void loop()
{
    bool bytesAvailable = false;
    if (PRIMARY_SERIAL_PORT.available())
    {
        comPort = (HardwareSerial*) &PRIMARY_SERIAL_PORT;
        bytesAvailable = true;
    }
    #if defined(SECONDARY_SERIAL_PORT)
    else if (SECONDARY_SERIAL_PORT.available())
    {
        comPort = (HardwareSerial*) &SECONDARY_SERIAL_PORT;
        bytesAvailable = true;
    }
    #endif
    if (bytesAvailable)
    {
        handle_command(comPort->readStringUntil('\n'));
    }
    #if defined(SUPPORT_EEPROM) && defined(MCU_PICO)
    // On the Pico, emulated EEPROM is only saved to flash when we call commit()
    eeprom_commit_at_interval(10000); // NB this only writes to flash if something has changed.
    #endif

    for (int i = 0; i < registered_loop_fn_count; i++)
        registered_loop_functions[i]();
}
#endif //ndef UNIT_TEST