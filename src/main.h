#ifndef MAIN_H
#include "config.h"
#include <Arduino.h>
#include <ComPort.h>
#ifdef SUPPORT_EEPROM
#include <EEPROM.h>
#else
#include "dummyEEPROM.h"
#endif
#ifdef DEBUG_ON
#define D(x) comPort->println(x)
#else
#define D(x)
#endif

#define END_COMMAND \
    {               \
        "", NULL    \
    }
#define CHECK_END_COMMAND(C) check_end_command(C)

struct Command
{
    const char * command;
    void (*run)(String);
};

inline bool check_end_command(Command c) { return strlen(c.command) > 0; }
inline bool check_end_command(Command * c) {return strlen(c->command) > 0; }
inline bool read_eeprom_bool(uint16_t address, bool default_value)
{
    int8_t temp_value;
    EEPROM.get(address, temp_value);
    if (temp_value < 0)
        return default_value;

    return temp_value != 0;
}


const Command end_command = END_COMMAND;
uint8_t parse_arguments(char ** arguments, String, uint8_t);
bool assert_exact_args(uint8_t parsed, uint8_t num_args);

void print_arg_difference(uint8_t parsed, uint8_t expected);
void free_parsed_arguments(char** arguments, uint8_t parsed);

extern void register_module(const Command commands[], void (*loop_fn)(void));

void get_version(String);
void get_board(String);
void get_modules(String);
void get_eeprom_last_commit(String);
void eeprom_commit(String);
void get_system_status(String);

#define MAIN_H
#endif