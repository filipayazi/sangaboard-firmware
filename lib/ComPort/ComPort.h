#ifndef COMPORT_H
#define COMPORT_H

#include <Arduino.h>

// comPort is a Serial-like object that we can switch.
extern HardwareSerial* comPort;

#endif